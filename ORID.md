## O
- In code review, we reviewed our homework, the teacher showed us how to refactor.
- We learnt about http, worked in pairs to learn REST, and practiced writing URL, request bodies and response bodies.
- We had introduction of several annotations like @RequestMapping, we can use it to map web requests to specific methods. 
- We the benefits and key points of the pair program. 
- Finally, we worked in pairs to complete today's assignment.
## R
- REST has always been familiar and unfamiliar to me.
## I
- Although refactoring method first is faster, changing the naming of each variable will also help you to understand the code especially in big project.
- We should refactor in small steps.
- I learnt a lot about programming from my partner still ashamed when I say I learned Java.
## D
- I must listen carefully this time.